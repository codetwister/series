package ro.codetwisters.series.android.ui.torrents

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.list_item_torrent.view.*
import ro.codetwisters.series.android.ui.common.recycler.GenericRecyclerItemView
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.controller.SeriesController
import ro.codetwisters.series.v3.R
import ro.codetwisters.series.xentities.torrent.TorrentResult

class ItemViewTorrent(context: Context) : LinearLayout(context), GenericRecyclerItemView {

    private val seriesController by inject<SeriesController>()

    lateinit var item: TorrentResult

    init {
        View.inflate(context, R.layout.list_item_torrent, this)
        setOnClickListener {
            //            seriesController.downloadTorrent(item.downloadUrl)
        }
    }

    override fun bind(item: Any) {
        this.item = item as TorrentResult
        torrent_name.text = item.name
        torrent_leechers.text = item.leechers.toString()
        torrent_seeders.text = item.seeders.toString()
        torrent_size.text = item.sizeMb
    }
}
