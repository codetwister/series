package ro.codetwisters.series.android.ui.common.recycler

interface GenericRecyclerItemView {
    fun bind(item: Any)
}
