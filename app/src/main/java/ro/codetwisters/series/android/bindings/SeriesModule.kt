package ro.codetwisters.series.android.bindings

import android.content.Context
import ro.codetwisters.series.android.io.network.filelist.FLApiClient
import ro.codetwisters.series.android.io.network.sheets.GoogleSheetsApiClient
import ro.codetwisters.series.android.io.network.tmdb.TmdbApiClient
import ro.codetwisters.series.android.io.storage.memory.FLCredentialStore
import ro.codetwisters.series.android.io.storage.memory.GoogleSheetCredentialStore
import ro.codetwisters.series.android.ui.googleapi.GoogleSheetsActivityDelegate
import ro.codetwisters.series.android.ui.navigation.NavigationHub
import ro.codetwisters.series.injection.bindLazy
import ro.codetwisters.series.injection.bindsTo
import ro.codetwisters.series.interfaceadapters.controller.SeriesController
import ro.codetwisters.series.interfaceadapters.gateway.series.EpisodesApiClient
import ro.codetwisters.series.interfaceadapters.gateway.series.SeriesApiClient
import ro.codetwisters.series.interfaceadapters.gateway.series.SeriesGatewayImpl
import ro.codetwisters.series.interfaceadapters.gateway.series.TorrentApiClient
import ro.codetwisters.series.interfaceadapters.presenter.SeriesListPresenter
import ro.codetwisters.series.usecases.series.common.SeriesGateway
import ro.codetwisters.series.usecases.series.refresh.FetchEpisodes
import ro.codetwisters.series.usecases.series.refresh.FetchSeries
import ro.codetwisters.series.xentities.SimpleCredential

class SeriesModule(context: Context) {
    init {

        GoogleSheetCredentialStore::class.java bindsTo GoogleSheetCredentialStore()

        NavigationHub::class.java bindsTo NavigationHub()

        GoogleSheetsActivityDelegate::class.java bindsTo GoogleSheetsActivityDelegate()

        val credentialStore = FLCredentialStore()
        // TODO save credentials to shared prefs and load it from there
        val prefs = context.getSharedPreferences("fl_prefs", Context.MODE_PRIVATE)
        credentialStore.set(SimpleCredential(prefs.getString("fl_user", "") ?: "", prefs.getString("fl_pass", "") ?: ""))

        FLCredentialStore::class.java bindsTo credentialStore

        SeriesController::class.java bindsTo SeriesController()
        SeriesListPresenter::class.java bindsTo SeriesListPresenter()
        FetchSeries::class.java bindsTo FetchSeries()
        FetchEpisodes::class.java bindsTo FetchEpisodes()

        SeriesGateway::class.java bindLazy { SeriesGatewayImpl() }
        SeriesApiClient::class.java bindLazy { GoogleSheetsApiClient() }
        EpisodesApiClient::class.java bindLazy { TmdbApiClient() }
        TorrentApiClient::class.java bindLazy { FLApiClient() }
    }
}
