package ro.codetwisters.series.android.io.network.sheets

import ro.codetwisters.series.android.io.storage.memory.GoogleSheetCredentialStore
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.gateway.series.SeriesApiClient
import ro.codetwisters.series.xentities.tvshows.TvShow

class GoogleSheetsApiClient: SeriesApiClient {

    private val credentialValueStore by inject<GoogleSheetCredentialStore>()

    override suspend fun getSeries(): List<TvShow> {
        return GetSeriesCall().query(credentialValueStore.get()!!)?.mapNotNull(this::processSeries) ?: emptyList()
    }

    private fun processSeries(showsLine: String): TvShow? {
            if (showsLine.length >= 3) {
                val data = showsLine.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val show = TvShow()
                show.title = data[0]
                try {
                    if (data.size > 2) {
                        show.lastWatchedSeason = Integer.parseInt(data[1].replace("[^0-9]".toRegex(), ""))
                    }
                } catch (e: NumberFormatException) {
                    show.lastWatchedSeason = 1
                }

                try {
                    if (data.size > 3) {
                        show.lastWatchedEpisode = Integer.parseInt(data[2].replace("[^0-9]".toRegex(), ""))
                    }
                } catch (e: NumberFormatException) {
                    show.lastWatchedEpisode = 1
                }
                return show
            }
        return null
    }

}
