package ro.codetwisters.series.android.ui.torrents

import android.content.Context
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ro.codetwisters.series.android.ui.common.recycler.GenericListPage
import ro.codetwisters.series.android.ui.navigation.NavigationHub
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.controller.SeriesController
import ro.codetwisters.series.xentities.OperationProgress
import ro.codetwisters.series.xentities.torrent.TorrentResult
import ro.codetwisters.series.xentities.tvshows.TvShow

class TorrentResultPage constructor(context: Context) : GenericListPage(context) {

    private val navigationHub by inject<NavigationHub>()

    private val seriesController by inject<SeriesController>()

    lateinit var show: TvShow

    init {
        registerViewtype(TorrentResult::class.java, ItemViewTorrent::class.java)
    }

    override fun onUserRefresh() {
        GlobalScope.launch {
            showProgress(OperationProgress(0f, 1f, 0f))
            setListItems(seriesController.getTorrents(show))
            showProgress(OperationProgress(0f, 1f, 1f))
        }
    }

    override fun onBackPressed(): Boolean {
        navigationHub.showSeries()
        return true
    }

    fun showProgress(operationProgress: OperationProgress) {
        setRefreshing(operationProgress.isInProgress)
    }
}
