package ro.codetwisters.series.android.ui.navigation

import ro.codetwisters.series.xentities.tvshows.TvShow

interface NavigationHandler {
    fun showTorrentSearchResults(item: TvShow)
    fun showSeries()
    fun showSeriesDetail(item: TvShow)
    fun showSettings()
}
