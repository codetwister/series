package ro.codetwisters.series.android.io.network.tmdb

import ro.codetwisters.series.interfaceadapters.gateway.series.EpisodesApiClient
import ro.codetwisters.series.xentities.tvshows.Episode

class TmdbApiClient: EpisodesApiClient {
    override suspend fun getEpisodes(imdbId: String): List<Episode> {
        return emptyList()
    }
}