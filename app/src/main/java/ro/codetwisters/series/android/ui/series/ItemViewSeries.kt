package ro.codetwisters.series.android.ui.series

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.list_item_series.view.*
import ro.codetwisters.series.android.ui.common.recycler.GenericRecyclerItemView
import ro.codetwisters.series.android.ui.navigation.NavigationHub
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.controller.SeriesController
import ro.codetwisters.series.v3.R
import ro.codetwisters.series.xentities.torrent.TorrentSearchFormatter
import ro.codetwisters.series.xentities.tvshows.TvShow
import java.util.*

class ItemViewSeries(context: Context) : LinearLayout(context), GenericRecyclerItemView {

    private val seriesController by inject<SeriesController>()

    private val navigationHub by inject<NavigationHub>()

    private val torrentSearchFormatter by inject<TorrentSearchFormatter>()

    lateinit var item: TvShow

    init {
        View.inflate(context, R.layout.list_item_series, this)
        setOnClickListener {
            //            seriesController!!.getTorrents(item)
            navigationHub.showSeriesDetail(item)
        }
    }

    override fun bind(item: Any) {
        this.item = item as TvShow
        series_title.text = item.title
        last_seen.text = String.format(Locale.getDefault(), lastSeenFormat, item.lastWatchedSeason, item.lastWatchedEpisode)
    }

    companion object {
        private val lastSeenFormat = "Season %d episode %d"
    }

}
