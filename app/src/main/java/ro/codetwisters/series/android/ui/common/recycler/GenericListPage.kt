package ro.codetwisters.series.android.ui.common.recycler

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import java.util.ArrayList
import java.util.HashMap

import kotlinx.android.synthetic.main.generic_list_page.view.*
import ro.codetwisters.series.android.ui.common.BaseCustomView
import ro.codetwisters.series.v3.R

abstract class GenericListPage protected constructor(context: Context) : BaseCustomView(context) {

    private val viewClasses = HashMap<Int, Class<*>>()
    private val viewTypes = HashMap<Class<*>, Int>()

    private val items : ArrayList<Any> = ArrayList()

    protected abstract fun onUserRefresh()

    protected fun setListItems(items: Collection<Any>) {
        this.items.clear()
        this.items.addAll(items)
        updateListState()
    }


    protected fun <I> registerViewtype(itemClass: Class<I>, viewClass: Class<out GenericRecyclerItemView>) {
        val currentIndex = viewTypes.size
        viewTypes[itemClass] = currentIndex
        viewClasses[currentIndex] = viewClass
    }

    protected fun setRefreshing(refreshing: Boolean) {
        swipe_to_refresh.isRefreshing = refreshing
    }

    protected fun updateListState() {
        val emptyList = items.size == 0
        list.visibility = if (emptyList) View.GONE else View.VISIBLE
        empty_state.visibility = if (emptyList) View.VISIBLE else View.GONE
        list.adapter!!.notifyDataSetChanged()
    }

    override fun setupView() {
        swipe_to_refresh.setOnRefreshListener { this.onUserRefresh() }
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = CustomAdapter()
    }

    private fun createView(viewClass: Class<*>): View {
        try {
            return viewClass.getConstructor(Context::class.java).newInstance(context) as View
        } catch (e: Exception) {
            throw RuntimeException("Viewclass " + viewClass.name + " error creating view: " + e.message, e)
        }

    }

    override fun layoutId(): Int {
        return R.layout.generic_list_page
    }

    private class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class CustomAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return ViewHolder(createView(viewClasses[viewType]!!))
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            (holder.itemView as GenericRecyclerItemView).bind(items.get(position))
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun getItemViewType(position: Int): Int {
            return viewTypes[items.get(position).javaClass]!!
        }
    }
}
