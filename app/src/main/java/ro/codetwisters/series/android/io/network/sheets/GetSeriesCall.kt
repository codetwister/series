package ro.codetwisters.series.android.io.network.sheets

import android.text.TextUtils
import com.google.api.services.sheets.v4.Sheets
import java.io.IOException
import java.util.*

class GetSeriesCall : GoogleSheetApiCall<List<String>>() {

    @Throws(IOException::class)
    override fun makeSheetsApiCall(mService: Sheets): List<String> {
        val spreadsheetId = "1jICUXJhUGxJFFb3of44vaxbEHyPyu-BiGWRfn8AkbXQ"
        val range = "Seriale!A2:I"
        val results = ArrayList<String>()
        val response = mService.spreadsheets().values()
                .get(spreadsheetId, range)
                .execute()
        val values = response.getValues()
        if (values != null) {
            for (row in values) {
                results.add(TextUtils.join(", ", row))
            }
        }
        return results
    }
}
