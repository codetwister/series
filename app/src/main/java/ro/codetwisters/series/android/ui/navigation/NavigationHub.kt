package ro.codetwisters.series.android.ui.navigation

import ro.codetwisters.series.xentities.tvshows.TvShow
import java.util.ArrayList

class NavigationHub : NavigationHandler {
    private val navigationHandlers = ArrayList<NavigationHandler>()

    fun registerHandler(navigationHandler: NavigationHandler) {
        navigationHandlers.add(navigationHandler)
    }

    fun unregisterHandler(navigationHandler: NavigationHandler) {
        navigationHandlers.remove(navigationHandler)
    }

    override fun showSeriesDetail(item: TvShow) {
        for (navigationHandler in navigationHandlers) {
            navigationHandler.showSeriesDetail(item)
        }
    }

    override fun showTorrentSearchResults(item: TvShow) {
        for (navigationHandler in navigationHandlers) {
            navigationHandler.showTorrentSearchResults(item)
        }
    }

    override fun showSeries() {
        for (navigationHandler in navigationHandlers) {
            navigationHandler.showSeries()
        }
    }

    override fun showSettings() {
        for (navigationHandler in navigationHandlers) {
            navigationHandler.showSettings()
        }
    }
}
