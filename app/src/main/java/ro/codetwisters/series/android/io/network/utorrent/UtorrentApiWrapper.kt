package ro.codetwisters.series.android.io.network.utorrent

import com.utorrent.webapiwrapper.core.UTorrentWebAPIClient
import com.utorrent.webapiwrapper.restclient.ConnectionParams


class UtorrentApiWrapper {

    fun testUtorrent() {
        val connectionParams = ConnectionParams.builder()
                .withScheme("http")
                .withCredentials("username", "password")
                .enableAuthentication(true)
                .withAddress("host.com", 8080)
                .withTimeout(1500)
                .create()
        val client = UTorrentWebAPIClient.getClient(connectionParams)

        val torrents = client.allTorrents
    }

    fun downloadTorrent(url: String?): Boolean {
        return false
    }

}
