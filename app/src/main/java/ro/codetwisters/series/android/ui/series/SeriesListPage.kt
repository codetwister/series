package ro.codetwisters.series.android.ui.series

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ro.codetwisters.series.android.ui.common.recycler.GenericListPage
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.controller.SeriesController
import ro.codetwisters.series.interfaceadapters.presenter.Completed
import ro.codetwisters.series.interfaceadapters.presenter.LoadingError
import ro.codetwisters.series.interfaceadapters.presenter.LoadingSeries
import ro.codetwisters.series.interfaceadapters.presenter.SeriesListPresenter
import ro.codetwisters.series.xentities.tvshows.TvShow

class SeriesListPage constructor(context: Context) : GenericListPage(context) {

    private val seriesController by inject<SeriesController>()
    private val seriesListPresenter by inject<SeriesListPresenter>()

    init {
        registerViewtype(TvShow::class.java, ItemViewSeries::class.java)
    }

    override fun onUserRefresh() {
        seriesController.fetchAllSeries()
    }

    // errors are handled another way
    @SuppressLint("RxSubscribeOnError")
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        GlobalScope.launch {
            seriesListPresenter.stream(this@SeriesListPage).collect {
                withContext(Dispatchers.Main) {
                    when (it) {
                        is Completed -> {
                            setListItems(it.list)
                            setRefreshing(false)
                        }
                        is LoadingError -> {
                            setRefreshing(false)
                            Toast.makeText(context, "Error ${it.e}", Toast.LENGTH_SHORT).show()
                        }
                        is LoadingSeries -> {
                            Toast.makeText(context, "Refreshing data", Toast.LENGTH_SHORT).show()
                            setRefreshing(true)
                        }
                    }
                }
            }

        }
        seriesController.fetchAllSeries()
    }

    override fun onDetachedFromWindow() {
        seriesListPresenter.stop(this)
        super.onDetachedFromWindow()
    }
}

