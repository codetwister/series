package ro.codetwisters.series.android.ui.googleapi

import android.Manifest
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.sheets.v4.SheetsScopes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import ro.codetwisters.series.android.io.storage.memory.GoogleSheetCredentialStore
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.presenter.Completed
import ro.codetwisters.series.interfaceadapters.presenter.LoadingError
import ro.codetwisters.series.interfaceadapters.presenter.LoadingSeries
import ro.codetwisters.series.interfaceadapters.presenter.SeriesListPresenter
import ro.codetwisters.series.interfaceadapters.presenter.SeriesListState
import java.util.*

class GoogleSheetsActivityDelegate {

    private val seriesListPresenter by inject<SeriesListPresenter>()

    private val credentialValueStore by inject<GoogleSheetCredentialStore>()

    private val activity by inject<Activity>()

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private val isGooglePlayServicesAvailable: Boolean
        get() {
            val apiAvailability = GoogleApiAvailability.getInstance()
            val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(activity)
            return connectionStatusCode == ConnectionResult.SUCCESS
        }

    fun onCreate() {
        attachErrorPresenter()
        // Initialize credentials and service object.
        val googleCredential = initializeCredentials()
        if (!isGooglePlayServicesAvailable) {
            acquireGooglePlayServices()
        } else if (googleCredential.selectedAccountName == null) {
            chooseAccount()
        }
    }

    private fun initializeCredentials(): GoogleAccountCredential {
        val googleCredential = GoogleAccountCredential.usingOAuth2(
                activity.applicationContext, Arrays.asList(*SCOPES))
                .setBackOff(ExponentialBackOff())
        credentialValueStore.set(googleCredential)
        return googleCredential
    }

    // this is an error handling branch, if this fails, it's very weird
    @SuppressLint("RxSubscribeOnError")
    private fun attachErrorPresenter() {
        GlobalScope.launch {
            seriesListPresenter.stream(this@GoogleSheetsActivityDelegate)
                    .collect {
                        withContext(Dispatchers.Main) {
                            when (it) {
                                is LoadingError -> this@GoogleSheetsActivityDelegate.onError(it.e)
                                else -> null
                            }
                        }
                    }
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GOOGLE_PLAY_SERVICES -> if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(activity, "Play services unavailable!", Toast.LENGTH_LONG).show()
            }
            REQUEST_ACCOUNT_PICKER -> if (resultCode == Activity.RESULT_OK && data != null &&
                    data.extras != null) {
                val accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
                if (accountName != null) {
                    val settings = activity.getPreferences(Context.MODE_PRIVATE)
                    val editor = settings.edit()
                    editor.putString(PREF_ACCOUNT_NAME, accountName)
                    editor.apply()
                    credentialValueStore.get()!!.selectedAccountName = accountName
                }
            }
            REQUEST_AUTHORIZATION -> if (resultCode == Activity.RESULT_OK) {
                // TODO after auth request is ok refresh data, but what data?
            }
        }

    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private fun chooseAccount() {
        if (EasyPermissions.hasPermissions(
                        activity, Manifest.permission.GET_ACCOUNTS)) {
            val accountName = activity.getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null)
            if (accountName != null) {
                credentialValueStore.get()!!.selectedAccountName = accountName
                // TODO after choose account, refresh data, but what data?
            } else {
                // Start a dialog from which the user can choose an account
                activity.startActivityForResult(
                        credentialValueStore.get()!!.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER)
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    activity,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS)
        }
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private fun acquireGooglePlayServices() {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(activity)
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     * Google Play Services on this device.
     */
    private fun showGooglePlayServicesAvailabilityErrorDialog(
            connectionStatusCode: Int) {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val dialog = apiAvailability.getErrorDialog(
                activity,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES)
        dialog.show()
    }

    private fun onError(error: Throwable?): Boolean {
        Toast.makeText(activity, "Error from delegate $error", Toast.LENGTH_SHORT).show()
        if (error != null) {
            if (error is GooglePlayServicesAvailabilityIOException) {
                showGooglePlayServicesAvailabilityErrorDialog(
                        error
                                .connectionStatusCode)
                return true
            } else if (error is UserRecoverableAuthIOException) {
                activity.startActivityForResult(
                        error.intent,
                        REQUEST_AUTHORIZATION)
                return true
            }
        }
        return true
    }

    fun onDestroy() {
        seriesListPresenter.stop(this)
    }

    companion object {

        private val PREF_ACCOUNT_NAME = "accountName"
        private val SCOPES = arrayOf(SheetsScopes.SPREADSHEETS_READONLY)

        private val REQUEST_ACCOUNT_PICKER = 1000
        private val REQUEST_AUTHORIZATION = 1001
        private val REQUEST_GOOGLE_PLAY_SERVICES = 1002
        private const val REQUEST_PERMISSION_GET_ACCOUNTS = 1003
    }
}
