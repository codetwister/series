package ro.codetwisters.series.android.ui.settings

import android.content.Context
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.fl_credentials.view.*
import ro.codetwisters.series.android.io.storage.memory.FLCredentialStore
import ro.codetwisters.series.android.ui.common.BaseCustomView
import ro.codetwisters.series.android.ui.navigation.NavigationHub
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.v3.R
import ro.codetwisters.series.xentities.SimpleCredential
import java.util.concurrent.TimeUnit

class SettingsPage constructor(context: Context) : BaseCustomView(context) {

    private val credentialStore by inject<FLCredentialStore>()

    private val navigationHub by inject<NavigationHub>()

    override fun setupView() {
        fl_username.setText(credentialStore.get()!!.username)
        fl_password.setText(credentialStore.get()!!.pass)
        RxView.clicks(save_fl_credentials_button)
                .throttleFirst(5, TimeUnit.SECONDS)
                .doOnNext { this.saveCredentials(fl_username.text.toString(), fl_password.text.toString()) }.subscribe()
    }

    private fun saveCredentials(username: String, password: String) {
        credentialStore.set(SimpleCredential(username, password))
        val prefs = context.getSharedPreferences("fl_prefs", Context.MODE_PRIVATE)
        prefs.edit().putString("fl_user", username).putString("fl_pass", password).apply()
    }

    override fun layoutId(): Int {
        return R.layout.fl_credentials
    }

    override fun onBackPressed(): Boolean {
        navigationHub.showSeries()
        return true
    }
}
