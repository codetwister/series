package ro.codetwisters.series.android.io.network.filelist

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler

import java.util.ArrayList

import ro.codetwisters.series.xentities.torrent.TorrentResult

class FileListParserHandler : DefaultHandler() {
    val resultList = ArrayList<TorrentResult>()
    private var currentItem: TorrentResult? = null
    private var currentColumnIndex: Int = 0

    private var chars = ""

    private var currentState = State.INITIALIZED
    private var divCount = 0

    private fun checkTag(localName: String, qName: String, expected: String): Boolean {
        return expected == localName || expected == qName
    }

    private fun checkTorrentTable(attributes: Attributes): Boolean {
        return searchInAttributes(attributes, "torrenttable")
    }

    private fun checkTorrentRow(attributes: Attributes): Boolean {
        return searchInAttributes(attributes, "torrentrow")
    }

    private fun searchInAttributes(attributes: Attributes, searchString: String): Boolean {
        for (i in 0 until attributes.length) {
            if (attributes.getValue(i) == searchString) {
                return true
            }
        }
        return false
    }

    private fun isTorrentRowStarting(localName: String, qName: String, attributes: Attributes): Boolean {
        return checkTag(localName, qName, "div") && checkTorrentRow(attributes)
    }

    private fun isTorrentInfoDiv(localName: String, qName: String, attributes: Attributes): Boolean {
        return checkTag(localName, qName, "div") && checkTorrentTable(attributes)
    }

    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
        super.startElement(uri, localName, qName, attributes)

        if (isTorrentRowStarting(localName, qName, attributes)) {
            setState(State.ROW_STARTED)
        } else if (isTorrentInfoDiv(localName, qName, attributes)) {
            currentColumnIndex++
        }

        if (currentState.ordinal >= State.ROW_STARTED.ordinal) {
            if (checkTag(localName, qName, "div")) {
                divCount++
                chars = ""
            }

            if (currentColumnIndex == COLUMN_LINK && checkTag(localName, qName, "a")) {
                for (i in 0 until attributes.length) {
                    if (checkTag(attributes.getLocalName(i), attributes.getQName(i), "href")) {
                        currentItem!!.downloadUrl = attributes.getValue(i)
                    }
                }
            }
        }
    }

    private fun setState(state: State) {
        if (currentState != state) {
            when (state) {
                FileListParserHandler.State.INITIALIZED -> {
                }
                FileListParserHandler.State.ROW_STARTED -> {
                    currentColumnIndex = 0
                    divCount = 0
                    currentItem = TorrentResult()
                    currentItem!!.torrentSource = "filelist"
                }
                FileListParserHandler.State.NAME_PARSED -> {
                    currentItem!!.name = chars
                    chars = ""
                }
                FileListParserHandler.State.LINK_PARSED -> {
                }
                FileListParserHandler.State.SIZE_PARSED -> {
                    currentItem!!.sizeMb = chars
                    chars = ""
                }
                FileListParserHandler.State.SEEDERS_PARSED -> {
                    currentItem!!.seeders = Integer.parseInt(chars)
                    chars = ""
                }
                FileListParserHandler.State.LEECHERS_PARSED -> {
                    currentItem!!.leechers = Integer.parseInt(chars)
                    chars = ""
                }
                FileListParserHandler.State.ROW_FINISHED -> resultList.add(currentItem!!)
            }
            currentState = state
        }
    }

    @Throws(SAXException::class)
    override fun endElement(uri: String, localName: String, qName: String) {
        super.endElement(uri, localName, qName)
        if (currentState.ordinal >= State.ROW_STARTED.ordinal) {
            if (checkTag(localName, qName, "div")) {
                divCount--
            }

            if (currentColumnIndex == COLUMN_SIZE && checkTag(localName, qName, "div")) {
                setState(State.SIZE_PARSED)
            }

            if (currentColumnIndex == COLUMN_TITLE && checkTag(localName, qName, "b")) {
                setState(State.NAME_PARSED)
            }

            if (currentColumnIndex == COLUMN_SEED && checkTag(localName, qName, "b")) {
                setState(State.SEEDERS_PARSED)
            }

            if (currentColumnIndex == COLUMN_LEECH && checkTag(localName, qName, "b")) {
                setState(State.LEECHERS_PARSED)
            }

            if (divCount == 0) {
                setState(State.ROW_FINISHED)
            }
        }
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray, start: Int, length: Int) {
        super.characters(ch, start, length)
        if (currentColumnIndex == COLUMN_SIZE ||
                currentColumnIndex == COLUMN_TITLE ||
                currentColumnIndex == COLUMN_SEED ||
                currentColumnIndex == COLUMN_LEECH) {
            chars += String(ch, start, length)
        }
    }

    private enum class State {
        INITIALIZED,
        ROW_STARTED,
        NAME_PARSED,
        LINK_PARSED,
        SIZE_PARSED,
        SEEDERS_PARSED,
        LEECHERS_PARSED,
        ROW_FINISHED

    }

    companion object {

        private val COLUMN_TITLE = 2
        private val COLUMN_LINK = 4
        private val COLUMN_SIZE = 7
        private val COLUMN_SEED = 9
        private val COLUMN_LEECH = 10
    }
}
