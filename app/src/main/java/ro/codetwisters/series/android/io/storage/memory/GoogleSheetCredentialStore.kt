package ro.codetwisters.series.android.io.storage.memory

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import ro.codetwisters.series.xentities.ValueStore

class GoogleSheetCredentialStore: ValueStore<GoogleAccountCredential>()