package ro.codetwisters.series.android.ui.series

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.series_details_page.view.*
import ro.codetwisters.series.android.ui.common.BaseCustomView
import ro.codetwisters.series.v3.R
import ro.codetwisters.series.xentities.tvshows.TvShow

class SeriesDetailPage constructor(context: Context) : BaseCustomView(context) {

    lateinit var show: TvShow

    override fun setupView() {
        pager.adapter = DemoCollectionAdapter()

        TabLayoutMediator(tab_layout, pager) { tab, position ->
            tab.text = "Season ${(position + 1)}"
        }.attach()
    }

    override fun layoutId() = R.layout.series_details_page
}

private class DemoCollectionAdapter: RecyclerView.Adapter<ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(TextView(parent.context).apply {
            setTextColor(Color.WHITE)
            gravity = Gravity.CENTER
            layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        })
    }

    override fun getItemCount() = 3

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.item.text = "Episodes list $position\nasdf\nasdf"
    }

}

class ItemViewHolder(val item: TextView): RecyclerView.ViewHolder(item)
