package ro.codetwisters.series.android.io.network.sheets

import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.sheets.v4.Sheets
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.IOException

abstract class GoogleSheetApiCall<T> {

    internal fun query(credential: GoogleAccountCredential): T? {
        return credential.selectedAccountName?.let { getData(credential) }
    }

    @Throws(IOException::class)
    protected abstract fun makeSheetsApiCall(mService: Sheets): T

    @Throws(IOException::class)
    private fun getData(credential: GoogleAccountCredential): T {
        val transport = AndroidHttp.newCompatibleTransport()
        val jsonFactory = JacksonFactory.getDefaultInstance()
        val service = Sheets.Builder(
                transport, jsonFactory, credential)
                .setApplicationName("Google Sheets API Android Quickstart")
                .build()

        return makeSheetsApiCall(service)
    }
}
