package ro.codetwisters.series.android.io.network.filelist

import android.net.Uri
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import org.xml.sax.helpers.XMLReaderFactory
import ro.codetwisters.series.xentities.SimpleCredential
import ro.codetwisters.series.xentities.torrent.TorrentResult
import java.io.*
import java.net.*
import java.util.*

/**
 * helper methods for file list stuff
 */
internal class FileListHelper(private val flCredential: SimpleCredential) {

    fun loginToFl() {
        try {

            val flUrl = URL("http://filelist.ro/takelogin.php")
            val conn = flUrl.openConnection() as HttpURLConnection
            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true

            val os = conn.outputStream
            val writer = BufferedWriter(
                    OutputStreamWriter(os, "UTF-8"))
            writer.write(String.format("username=%s&password=%s", flCredential.username, flCredential.pass))
            writer.flush()
            writer.close()
            os.close()

            conn.connect()
            val br = BufferedReader(InputStreamReader(conn.inputStream))
            // TODO there must be a more stable alternative to do this.

            while (br.readLine() != null) {
                // NO-OP but for some reason we must do this
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun searchTorrent(searchTerm: String): ArrayList<TorrentResult> {

        try {
            val browseUrl = URL(String.format("http://filelist.ro/browse.php?search=%s&cat=0&searchin=0&sort=0", Uri.encode(searchTerm)))
            val urlConnection = browseUrl.openConnection() as HttpURLConnection

            val xr = XMLReaderFactory.createXMLReader("org.ccil.cowan.tagsoup.Parser")
            val handler = FileListParserHandler()
            xr.contentHandler = handler
            val inStream = InputSource()

            inStream.byteStream = urlConnection.inputStream

            xr.parse(inStream)
            return handler.resultList
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: SAXException) {
            e.printStackTrace()
        }

        return ArrayList()
    }

    companion object {

        init {
            val cookieManager = CookieManager()
            CookieHandler.setDefault(cookieManager)
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        }
    }

}
