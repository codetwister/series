package ro.codetwisters.series.android.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.system.Os.bind
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.main.*
import ro.codetwisters.series.android.bindings.SeriesModule
import ro.codetwisters.series.android.ui.common.BaseCustomView
import ro.codetwisters.series.android.ui.googleapi.GoogleSheetsActivityDelegate
import ro.codetwisters.series.android.ui.navigation.NavigationHandler
import ro.codetwisters.series.android.ui.navigation.NavigationHub
import ro.codetwisters.series.android.ui.series.SeriesDetailPage
import ro.codetwisters.series.android.ui.series.SeriesListPage
import ro.codetwisters.series.android.ui.settings.SettingsPage
import ro.codetwisters.series.android.ui.torrents.TorrentResultPage
import ro.codetwisters.series.injection.bindsTo
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.v3.R
import ro.codetwisters.series.xentities.tvshows.TvShow


class MainActivity : AppCompatActivity() {

    private val delegate by inject<GoogleSheetsActivityDelegate>()

    private val navigationHub by inject<NavigationHub>()

    private val mainNavigationHandler: NavigationHandler = MainScreenNavigationHandler()

    private var currentPage: BaseCustomView? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main)

        SeriesModule(applicationContext)
        Activity::class.java bindsTo this

        navigationHub.registerHandler(mainNavigationHandler)
        navigationHub.showSeries()

        setSupportActionBar(my_toolbar as Toolbar)
        supportActionBar!!.show()

        delegate.onCreate()
    }

    override fun onDestroy() {
        navigationHub.unregisterHandler(mainNavigationHandler)
        delegate.onDestroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (currentPage!!.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        delegate.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        delegate.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.getItemId()) {
            R.id.settings -> {
                navigationHub.showSettings()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private inner class MainScreenNavigationHandler : NavigationHandler {
        override fun showSeriesDetail(item: TvShow) {
            val page = SeriesDetailPage(this@MainActivity)
            page.show = item
            setCurrentPage(page)
        }

        override fun showSettings() {
            setCurrentPage(SettingsPage(this@MainActivity))
        }

        override fun showTorrentSearchResults(item: TvShow) {
            val page = TorrentResultPage(this@MainActivity)
            page.show = item
            setCurrentPage(page)
        }

        override fun showSeries() {
            setCurrentPage(SeriesListPage(this@MainActivity))
        }

        private fun setCurrentPage(page: BaseCustomView) {
            currentPage = page
            container!!.removeAllViews()
            container!!.addView(currentPage)
        }
    }
}
