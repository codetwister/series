package ro.codetwisters.series.android.ui.common

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout

abstract class BaseCustomView(context: Context) : FrameLayout(context) {

    init {
        LayoutInflater.from(context).inflate(layoutId(), this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setupView()
    }

    protected abstract fun setupView()

    protected abstract fun layoutId(): Int

    open fun onBackPressed(): Boolean {
        return false
    }
}
