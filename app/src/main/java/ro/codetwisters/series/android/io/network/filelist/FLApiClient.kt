package ro.codetwisters.series.android.io.network.filelist

import ro.codetwisters.series.android.io.network.utorrent.UtorrentApiWrapper
import ro.codetwisters.series.android.io.storage.memory.FLCredentialStore
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.gateway.series.TorrentApiClient
import ro.codetwisters.series.xentities.torrent.TorrentResult

class FLApiClient: TorrentApiClient {
    private val credentialValueStore by inject<FLCredentialStore>()
    private val utorrentWrapper by inject<UtorrentApiWrapper>()

    override suspend fun downloadTorrent(url: String?): Boolean {
        return utorrentWrapper.downloadTorrent(url)
    }

    override suspend fun getTorrents(searchString: String?): List<TorrentResult> {
        return fetchTorrentResults(searchString)
    }

    private fun fetchTorrentResults(searchString: String?): List<TorrentResult> {
        val flHelper = FileListHelper(credentialValueStore.get()!!)
        flHelper.loginToFl()
        return flHelper.searchTorrent(searchString!!)
    }
}
