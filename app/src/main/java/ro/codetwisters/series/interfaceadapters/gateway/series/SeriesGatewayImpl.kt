package ro.codetwisters.series.interfaceadapters.gateway.series

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.usecases.series.common.SeriesGateway
import ro.codetwisters.series.xentities.torrent.TorrentResult
import ro.codetwisters.series.xentities.tvshows.Episode
import ro.codetwisters.series.xentities.tvshows.TvShow

class SeriesGatewayImpl: SeriesGateway {

    private val seriesApiClient by inject<SeriesApiClient>()
    private val episodesApiClient by inject<EpisodesApiClient>()
    private val torrentApiClient by inject<TorrentApiClient>()

    override suspend fun getSeries() = withContext(Dispatchers.IO) {
        seriesApiClient.getSeries()
    }

    override suspend fun getEpisodes(imdbId: String) = withContext(Dispatchers.IO) {
        episodesApiClient.getEpisodes(imdbId)
    }

    override suspend fun getTorrents(searchString: String?): List<TorrentResult> {
        return torrentApiClient.getTorrents(searchString)
    }

    override suspend fun downloadTorrent(url: String?): Boolean {
        return torrentApiClient.downloadTorrent(url)
    }

}
