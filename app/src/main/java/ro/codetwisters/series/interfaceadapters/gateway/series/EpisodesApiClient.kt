package ro.codetwisters.series.interfaceadapters.gateway.series

import ro.codetwisters.series.xentities.tvshows.Episode

interface EpisodesApiClient {
    suspend fun getEpisodes(imdbId: String): List<Episode>
}
