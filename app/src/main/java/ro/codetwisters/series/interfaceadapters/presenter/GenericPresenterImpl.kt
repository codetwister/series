package ro.codetwisters.series.interfaceadapters.presenter

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow

class GenericPresenterImpl<T>: Presenter<T> {
    private val consumerMap = mutableMapOf<Any, Channel<T>>()

    override fun stream(consumer: Any): Flow<T> {
        return consumerMap.getOrPut(consumer, { Channel() }).consumeAsFlow()
    }

    override fun stop(consumer: Any) {
        consumerMap.remove(consumer)?.cancel()
    }

    override suspend fun accept(t: T) {
        consumerMap.values.forEach { it.send(t) }
    }

}