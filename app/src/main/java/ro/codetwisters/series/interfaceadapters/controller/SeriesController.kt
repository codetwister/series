package ro.codetwisters.series.interfaceadapters.controller

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.interfaceadapters.presenter.Completed
import ro.codetwisters.series.interfaceadapters.presenter.LoadingError
import ro.codetwisters.series.interfaceadapters.presenter.LoadingSeries
import ro.codetwisters.series.interfaceadapters.presenter.SeriesListPresenter
import ro.codetwisters.series.usecases.series.availability.CheckEpisodeAvailable
import ro.codetwisters.series.usecases.series.download.DownloadTorrent
import ro.codetwisters.series.usecases.series.refresh.FetchSeries
import ro.codetwisters.series.xentities.torrent.TorrentSearchFormatter
import ro.codetwisters.series.xentities.tvshows.TvShow

class SeriesController {

    private val fetchSeries by inject<FetchSeries>()
    private val checkEpisodeAvailable by inject<CheckEpisodeAvailable>()
    private val downloadTorrent by inject<DownloadTorrent>()

    private val torrentSearchFormatter by inject<TorrentSearchFormatter>()
    private val seriesListPresenter by inject<SeriesListPresenter>()

    fun fetchAllSeries() {
        GlobalScope.launch {
            seriesListPresenter.accept(LoadingSeries)
            try {
                seriesListPresenter.accept(Completed(fetchSeries.execute()))
            } catch (e: Exception) {
                seriesListPresenter.accept(LoadingError(e))
            }
        }
    }

    suspend fun getTorrents(show: TvShow) = checkEpisodeAvailable.execute(torrentSearchFormatter.getNextEpisodeSearchString(show))

    suspend fun downloadTorrent(downloadUrl: String?) = downloadTorrent.execute(downloadUrl)
}
