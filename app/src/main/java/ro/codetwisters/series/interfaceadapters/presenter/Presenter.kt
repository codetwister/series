package ro.codetwisters.series.interfaceadapters.presenter

import kotlinx.coroutines.flow.Flow

interface Presenter<T> {
    fun stream(consumer: Any): Flow<T>
    fun stop(consumer: Any)
    suspend fun accept(t: T)
}