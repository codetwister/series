package ro.codetwisters.series.interfaceadapters.gateway.series

import ro.codetwisters.series.xentities.tvshows.TvShow

interface SeriesApiClient {
    suspend fun getSeries(): List<TvShow>
}
