package ro.codetwisters.series.interfaceadapters.presenter

import ro.codetwisters.series.xentities.tvshows.TvShow

class SeriesListPresenter: Presenter<SeriesListState> by GenericPresenterImpl()

sealed class SeriesListState
object LoadingSeries : SeriesListState()
data class LoadingError(val e: Exception): SeriesListState()
data class Completed(val list: List<TvShow>): SeriesListState()
