package ro.codetwisters.series.injection

import java.lang.Exception

object Injection {

    private val factoryMap = mutableMapOf<Class<out Any>, () -> Any>()

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> getInstance(type: Class<T>): T = factoryMap[type]?.invoke()?.let { it as T } ?: try { type.newInstance() } catch (e: Exception) { throw Exception("Could not find factory or create instance for $type") }

    fun <T: Any> bindTo(clazz: Class<T>, instance: T) {
        factoryMap[clazz] = { instance }
    }

    fun <T: Any> bindLazy(clazz: Class<T>, factory: () -> T) {
        factoryMap[clazz] = factory
    }

}
