package ro.codetwisters.series.injection

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class Injector<out T: Any>(private val type: Class<T>): ReadOnlyProperty<Any, T> {
    override fun getValue(thisRef: Any, property: KProperty<*>): T = Injection.getInstance(type)
}

inline fun <reified T: Any> inject(): Injector<T> = Injector(T::class.java)

infix fun <T: Any> Class<T>.bindsTo(instance: T) {
    Injection.bindTo(this, instance)
}

infix fun <T: Any> Class<T>.bindLazy(factory: () -> T) {
    Injection.bindLazy(this, factory)
}
