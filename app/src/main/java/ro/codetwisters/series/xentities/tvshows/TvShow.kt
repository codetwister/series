package ro.codetwisters.series.xentities.tvshows

data class TvShow(
    var title: String? = null,
    var imdbId: String? = null,
    var lastWatchedSeason: Int = 0,
    var lastWatchedEpisode: Int = 0)
