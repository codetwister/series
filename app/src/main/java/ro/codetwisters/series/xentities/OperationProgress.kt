package ro.codetwisters.series.xentities

class OperationProgress(val minValue: Float, val maxValue: Float, val progressValue: Float) {
    val isInProgress: Boolean
        get() = progressValue < maxValue
}
