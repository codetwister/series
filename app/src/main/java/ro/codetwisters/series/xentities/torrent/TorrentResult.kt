package ro.codetwisters.series.xentities.torrent

data class TorrentResult(
    var torrentSource: String? = null,
    var name: String? = null,
    var downloadUrl: String? = null,
    var sizeMb: String? = null,
    var seeders: Int = 0,
    var leechers: Int = 0)

