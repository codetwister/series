package ro.codetwisters.series.xentities.torrent

import java.text.DecimalFormat

import ro.codetwisters.series.xentities.tvshows.TvShow

class TorrentSearchFormatter {

    fun getNextEpisodeSearchString(tvShow: TvShow): String {
        return String.format(
                nextEpisodeFormat,
                tvShow.title,
                df.format(tvShow.lastWatchedSeason.toLong()),
                df.format((tvShow.lastWatchedEpisode + 1).toLong()))
    }

    companion object {
        private val df = DecimalFormat("00")
        private val nextEpisodeFormat = "%s S%sE%s"
    }

}
