package ro.codetwisters.series.xentities.tvshows

data class Episode(val imdbId: String, val seasonNumber: Int, val episodeNumber: Int, val title: String, val watched: Boolean)