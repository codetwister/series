package ro.codetwisters.series.xentities

open class ValueStore<T> {

    private var value: T? = null
    fun set(value: T) {
        this.value = value
    }

    fun get(): T? {
        return value
    }

}
