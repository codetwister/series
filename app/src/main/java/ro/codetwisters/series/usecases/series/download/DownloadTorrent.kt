package ro.codetwisters.series.usecases.series.download

import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.usecases.series.common.SeriesGateway

class DownloadTorrent {

    private val seriesGateway by inject<SeriesGateway>()

    suspend fun execute(input: String?) = seriesGateway.downloadTorrent(input)
}
