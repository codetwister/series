package ro.codetwisters.series.usecases.series.availability

import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.usecases.series.common.SeriesGateway

class CheckEpisodeAvailable {

    private val seriesGateway by inject<SeriesGateway>()

    suspend fun execute(input: String?) = seriesGateway.getTorrents(input)

}
