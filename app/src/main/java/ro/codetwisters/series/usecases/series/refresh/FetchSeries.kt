package ro.codetwisters.series.usecases.series.refresh

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.usecases.series.common.SeriesGateway

class FetchSeries {

    private val seriesGateway by inject<SeriesGateway>()

    suspend fun execute() = withContext(Dispatchers.IO) {
        seriesGateway.getSeries()
    }
}
