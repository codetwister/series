package ro.codetwisters.series.usecases.series.refresh

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ro.codetwisters.series.injection.inject
import ro.codetwisters.series.usecases.series.common.SeriesGateway

class FetchEpisodes {

    private val seriesGateway by inject<SeriesGateway>()

    suspend fun execute(imdbId: String) = withContext(Dispatchers.IO) {
        seriesGateway.getEpisodes(imdbId)
    }
}
