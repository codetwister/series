package ro.codetwisters.series.usecases.series.common

import ro.codetwisters.series.xentities.torrent.TorrentResult
import ro.codetwisters.series.xentities.tvshows.Episode
import ro.codetwisters.series.xentities.tvshows.TvShow
import java.util.*

interface SeriesGateway {
    suspend fun getSeries(): List<TvShow>
    suspend fun getEpisodes(imdbId: String): List<Episode>
    suspend fun getTorrents(searchString: String?): List<TorrentResult>
    suspend fun downloadTorrent(url: String?): Boolean
}
