package ro.codetwisters.series.tests.fl;

import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

import ro.codetwisters.series.android.io.network.filelist.FileListParserHandler;
import ro.codetwisters.series.xentities.torrent.TorrentResult;

public class TestFlParsing {

    @Test
    public void testFlParserSuccess() throws IOException {
        XMLReader xr = null;
        try {
            xr = XMLReaderFactory.createXMLReader("org.ccil.cowan.tagsoup.Parser");
        } catch (SAXException e) {
            e.printStackTrace();
        }
        FileListParserHandler handler = new FileListParserHandler();
        xr.setContentHandler(handler);
        InputSource inStream = new InputSource();

        inStream.setByteStream(getClass().getClassLoader().getResourceAsStream("./flParseTestSuccess.html"));

        try {
            xr.parse(inStream);
            if (handler.getResultList().size() == 0) {
                System.out.println("NOTHING FOUND");
            }
            for (TorrentResult result : handler.getResultList()) {
                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

}
